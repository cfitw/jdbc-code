package com.java;

import org.junit.Test;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.SQLException;
import java.util.Properties;

//?serverTimezone=UTC代码时区报错，需要这样代码。第一个链接符用？，后面所使用的链接符为&。
public class FirstConnectionTest {
    @Test
    public void Connection() throws SQLException {
        /**
         * 如果安装的mysql版本8的话，需要加载的jdbc驱动是com.mysql.cj.jdbc.Driver()
         * 如果安装的mysql版本5，需要加载的jdbc驱动是com.mysql.jdbc.Driver()
         */
        //获取Driver的实现类对象
        Driver driver = new com.mysql.jdbc.Driver();
        //定位到具体的数据库
        //jdbc:mysql具体协议
        //localhost：ip地址
        //3306：默认的mysql的端口号
        //test：mysql的数据库
        String url = "jdbc:mysql://localhost:3306/test?serverTimezone=UTC";
        Properties info = new Properties();
        //设置用户名
        info.setProperty("user","root");
        //设置密码
        info.setProperty("password","123456");
        //建立连接
        Connection connection = driver.connect(url,info);
        System.out.println(connection);
    }

}
