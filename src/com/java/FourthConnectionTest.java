package com.java;

import org.junit.Test;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

/***
* 实现数据域代码的分类，可移植性和可维护性强
* 实现代码解耦。
* 如果需要修改文件配置信息，不需要重新打包程序代码
*/
public class FourthConnectionTest {
    @Test
    public void connectionTest04() throws Exception {
    //读取配置文件的四个基本信息
     InputStream input = FourthConnectionTest.class.
             getClassLoader().getResourceAsStream("jdbc.properties");
     Properties properties = new Properties();
     // 把配置文件加载进Properties
     properties.load(input);
     // 获取需要的基本信息
     String user = properties.getProperty("user");
     String password = properties.getProperty("password");
     String url = properties.getProperty("url");
     String driver = properties.getProperty("driverClass");
     //加载驱动
        Class.forName(driver);
     //获取连接
        Connection connection = DriverManager.getConnection(url, user, password);
        System.out.println(connection);
    }
}
