package com.java;

import java.io.InputStream;
import java.sql.*;
import java.util.Properties;

/**
 * 获取数据库的链接
 */
public class JDBCUtilTools {
    public static Connection getConnection() throws Exception{
        InputStream inputStream = ClassLoader.getSystemClassLoader()
                .getResourceAsStream("jdbc.properties");
        Properties properties = new Properties();
        properties.load(inputStream);
        String url = properties.getProperty("url");
        String user = properties.getProperty("user");
        String password = properties.getProperty("password");
        String driver = properties.getProperty("driverClass");
        Class.forName(driver);
        return  DriverManager.getConnection(url, user, password);
    }

    /**
     * 关闭资源的操作
     */
    public static void CloseConnection(Connection connection, Statement statement){
        try {
            if(connection != null)
                connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            if (statement != null)
                statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void CloseConnection01(Connection connection, Statement statement, ResultSet resultSet){
        try{
            if(connection != null){
                connection.close();
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        try{
            if(statement != null){
                statement.close();
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        try{
            if(resultSet != null){
                resultSet.close();
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}
