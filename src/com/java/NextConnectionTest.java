package com.java;

import org.junit.Test;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.SQLException;
import java.util.Properties;

public class NextConnectionTest {
    @Test
    public void Connection() throws Exception{
        /**
         * jdbc驱动程序的第二种模式：反射，通过获取Driver的实现类对象，
         * 优点是连接的方式更具有同用性，不会出现第三方的api，移植性强。
         */
        Class clazz = Class.forName("com.mysql.jdbc.Driver");
        Driver driver = (Driver) clazz.newInstance();
        String url = "jdbc:mysql://localhost:3306/test?serverTimezone=UTC";
        Properties info = new Properties();
        info.setProperty("user","root");
        info.setProperty("password","123456");
        Connection connection = driver.connect(url,info);
        System.out.println(connection);
    }
}
