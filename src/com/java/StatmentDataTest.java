package com.java;

/**
 * ORM 的编程思想（Object relational mapping)对象关系印射
 * 一个数据表对应一个java类
 * 表中的一条数据，对应java类的一个对象
 * 表中的一个字段，对应java类的一个属性。
 */
public class StatmentDataTest {
    private int id;
    private String name;
    private String work;

    public StatmentDataTest(){
        super();
    }

    public StatmentDataTest(int id,String name,String work){
        super();
        this.id = id;
        this.name = name;
        this.work = work;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWork() {
        return work;
    }

    public void setWork(String work) {
        this.work = work;
    }

    @Override
    public String toString() {
        return "StatmentDataTest{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", work='" + work + '\'' +
                '}';
    }
}
