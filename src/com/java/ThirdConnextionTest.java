package com.java;

import org.junit.Test;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;

public class ThirdConnextionTest {
    @Test
    public void ConnectionTest03() throws Exception{
        /**
         * 第三种jdbc驱动模式(使用DriverManager替换Driver,以下三行代码可以优化成一行)
         * Class clazz = Class.forName("com.mysql.cj.jdbc.Driver");
         * Driver driver = (Driver) clazz.newInstance();
         * DriverManager.registerDriver(driver);
         */
        // 这一行也可以省，具体原因分析驱动包下的../META-INF/services/java.sql.Driver
        Class.forName("com.mysql.jdbc.Driver");
        String user = "root";
        String password = "123456";
        String url = "jdbc:mysql://localhost:3306/test?serverTimezone=UTC";
        Connection connection = DriverManager.getConnection(url, user, password);
        System.out.println(connection);
    }
}
